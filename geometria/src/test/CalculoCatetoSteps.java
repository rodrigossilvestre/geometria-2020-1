package geometria;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class CalculoCatetoSteps {

	TrianguloIntegradoSelenium triang = new TrianguloIntegradoSelenium();
	
	@Given("estabeleci o comeco de calculo de triangulos")
	public void inicar() {
		triang.getIn();
	}

	@When("Estou selecionando o tipo de calculo para o Cateto")
	public void chooseCalc() {
		triang.selectCateto();
	}

	@When("estabeleci um cateto para c1")
	public void informarCateto1(String c1) {
		triang.cateto1(c1);
	}

	@When("estabeleci uma hipotenusa para o valor da hipotenusa ")
	public void informHipotenusa(String h) {
		triang.hipotenusa(h);
		;
	}

	@When("requeri que o calculo seja realizado")
	public void calc() {
		triang.calc();
	}

	@Then("O calculo do cateto foi realizado, e informado o valor")
	public void verificarValor(String c2) {
		Assert.assertEquals(c2, triang.getHipotenusa());
	}

}
