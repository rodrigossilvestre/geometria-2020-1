package geometria;

import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CalculoFirefoxTest extends TrianguloIntegradoSelenium {
		@BeforeAll
		public static void setup() {
			System.setProperty("webdriver.gecko.driver","./geckodriver.exe");
			driver = new FirefoxDriver();
		}
	}
