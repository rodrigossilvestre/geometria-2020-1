package geometria;

import org.jbehave.core.annotations.Then;

public class CalculoHipotenusaSteps {

	TrianguloIntegradoSelenium triang = new TrianguloIntegradoSelenium();

	@Given("estabeleci o comeco de calculo de triangulos")
	public void inicar() {
		triang.getIn();
	}

	@When("Estou selecionando o tipo de calculo para a Hipotenusa")
	public void escolherHipotenusa() {
		triang.selectHipotenusa();
	}

	@When("estabeleci um cateto para c1")
	public void informC1(String c1) {
		triang.cateto1(c1);
	}

	@When("estabeleci um cateto para c2")
	public void informC2(String c2) throws InterruptedException {
		triang.cateto2(c2);
	}

	@When("requeri que o calculo seja realizado")
	public void calcular() {
		triang.calc();
	}

	@Then("O calculo da hipotenusa foi realizado, e informado o valor")
	public void verify(String h) {
		String result = triang.getHipotenusa();
		System.out.println("\n " + result);
		Assert.assertEquals(h, result);
	}

}
