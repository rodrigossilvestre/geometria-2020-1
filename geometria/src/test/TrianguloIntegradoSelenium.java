package codigos;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TrianguloIntegradoSelenium {

	public static WebDriver driver;

	@AfterAll
	public static void teardown() {
		driver.quit();
	}

	@Test
	public void getIn() {
		System.setProperty("webdriver.chrome.driver", "E:/Users/rodri/git/geometria-2020-1/geometria/chromedriver.exe");
		driver.get("E:/Users/rodri/git/geometria-2020-1/geometria/src/main/webapp/triangulo.html");
	}

	@Test
	public void cateto1(String c1) {
		WebElement inputC1 = driver.findElement(By.id("cateto1"));
		inputC1.sendKeys(c1);
		Thread.sleep(10000);
	}

	@Test
	public void cateto2(String c2) throws InterruptedException {
		WebElement inputC2 = driver.findElement(By.id("cateto2"));
		inputC2.sendKeys(c2);
		Thread.sleep(10000);
	}

	@Test
	public void hipotenusa(String h) {
		WebElement inputH = driver.findElement(By.id("hipotenusa"));
		inputH.sendKeys(h);
		Thread.sleep(10000);
	}

	@Test
	public void calc() {
		WebElement button = driver.findElement(By.id("calcularBtn"));
		button.click();
		Thread.sleep(10000);
	}

	@Test
	public void selectHipotenusa() {
		Select select = new Select(driver.findElement(By.id("tipoCalculoSelect")));
		select.selectByVisibleText("Hipotenusa");
		Thread.sleep(10000);
	}

	@Test
	public void selectCateto() {
		Select select = new Select(driver.findElement(By.id("tipoCalculoSelect")));
		select.selectByVisibleText("Cateto");
		Thread.sleep(10000);
	}

	@Test
	public String getHipotenusa() {
		String h = "";
		WebElement hValue = driver.findElement(By.id("hipotenusa"));
		h = hValue.getText();
		return h;
		Thread.sleep(10000);
	}

	@Test
	public String getCateto() {
		String c = "";
		WebElement cValue = driver.findElement(By.id("cateto2"));
		c = cValue.getText();
		return c;
		Thread.sleep(10000);
	}

}
